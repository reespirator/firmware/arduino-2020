
#ifndef _REESPIRATOR_LOOP_HPP
#define _REESPIRATOR_LOOP_HPP

#include <LoopTicker.hpp>


// forward functions
void reset_watchdog(const void* object_ptr, LoopTicker* loop_ticker);


// LoopTicker tasks
static const LoopTicker::TaskEntryPoint loop_tasks[] =
{
    // add here the task entry-points to execute in the main loop
    { .function = reset_watchdog, .object_ptr = nullptr },
};

#define LOOP_TASKS_COUNT (sizeof(loop_tasks) / sizeof(loop_tasks[0]))


// reset the hardware watchdog
void reset_watchdog(const void* object_ptr, LoopTicker* loop_ticker)
{
    // ToDo
}


#endif // _REESPIRATOR_LOOP_HPP
