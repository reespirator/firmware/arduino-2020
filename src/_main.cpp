
//////////////////////////////////////////////////////////////////////
// headers
//////////////////////////////////////////////////////////////////////

#ifdef ARDUINO
#include <Arduino.h>
#endif

#include "reespirator_loop.h"

//////////////////////////////////////////////////////////////////////
// main program
//////////////////////////////////////////////////////////////////////

// LoopTicker as scheduler
static LoopTicker task_ticker(loop_tasks, LOOP_TASKS_COUNT);

void setup()
{
}

void loop()
{
    task_ticker.doLoop();
}

// simulate setup-loop pattern
#ifndef ARDUINO
int main()
{
    setup();
    for (;;) loop();
}
#endif


